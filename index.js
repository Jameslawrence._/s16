
let fruits = ['Banana','Mango'];
let index = 0; // number of the iteration, number of times of how many we repeat our code.

while(fruits.length > index){
	/*
		block of code
		counter iteration
	*/
	console.log(fruits[index]);
	index++;
}


let countB = 6;
do{
	console.log(`Do while count ${countB}`);
	countB--;
}while(countB == 7);

while(countB == 7){
	console.log(`while count ${countB}`);
	countB--;
}

let indexNumber = 0;
let computerBrands = ['Apple','Mac Pro','HP Notebook','Asus'];

do{
	console.log(computerBrands[indexNumber]);
	indexNumber++;
}while( computerBrands.length > indexNumber);

let colors = ['red','green','blue','yellow','purple'];
let indexColor = 0;

for(indexColor = 0; colors.length > indexColor; indexColor++){
	console.log(colors[indexColor]);
}


let ages = [18, 19, 20, 21, 22, 23];

for(let i = 0; i < ages.length; i++){
	if(ages[i] == 18 || ages[i] == 21){
		continue;
	}
	console.log(ages[i]);
}

let studentNames = ['james','george','camille'];
let studentIndex = 0;

for(studentIndex = 0; studentIndex < studentNames.length; studentIndex++){
	if(studentNames[studentIndex] == "george"){
		console.log(studentNames[studentIndex]);
		break;
	}
}
